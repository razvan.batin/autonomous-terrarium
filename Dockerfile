FROM node:11.15.0-alpine

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY package.json /usr/src/app/
RUN yarn install

COPY . /usr/src/app

ENV PORT 8080
CMD [ "yarn", "start" ]
