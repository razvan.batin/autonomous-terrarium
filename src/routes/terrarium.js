const express = require('express')
const Terrarium = require('../db/terrarium')
const router = express.Router()

router.post('/', async (req, res) => {
    const terrarium = new Terrarium(req.body)
    await terrarium.save()
    res.json(terrarium.toObject({versionKey: false}))
})

router.get('/:tid', async (req, res) => {
    const terrarium = await Terrarium.findOne({_id: req.params.tid})
    res.json(terrarium.toObject({versionKey: false}))
})

module.exports = router