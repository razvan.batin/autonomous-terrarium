const express = require('express')
const Data = require('../db/data')
const request = require('superagent')
const router = express.Router()


router.post('/', async (req, res) => {
    const data = new Data(req.body)
    await data.save()
    const obj = data.toObject({versionKey: false})

    if(process.env.SUMOLOGIC_URL) {
        await request
            .post(process.env.SUMOLOGIC_URL)
            .send(JSON.stringify(obj))
    }

    res.json(obj)
})

router.get('/:did', async (req, res) => {
    const data = await Data.findOne({_id: req.params.did})
    res.json(data.toObject({versionKey: false}))
})

module.exports = router